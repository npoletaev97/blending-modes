# Blending modes

Blend modes (or mixing modes) in digital image editing and computer graphics are used to determine how two layers are blended with each other.
This app allows you to see how work different blending modes

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
